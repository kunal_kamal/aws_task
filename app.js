const express = require('express');

const app = express();

app.get('/',(req,res) => res.send("Welcome to Homepage!"));

app.get('/user', (req,res) => res.send('Welcome User!'));

app.get('/health', (req,res) => res.send('Healthy!'))

app.listen(3000, () => console.log('Server is running...'))